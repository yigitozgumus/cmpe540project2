#include"Agent.h"
#include"Util.h"
using namespace std;
extern Environment *env;
extern std::vector<Agent *> agents;           // Agents. the first agent is our agent, the others are enemy agents.

// creates the agent, sets the search algorithm of the agent
// do not change
Agent::Agent (int index, char searchStr[], int maxSearchDepth){
		if (!strcmp(searchStr,"RAND"))
				searchType = RAND;
		else if (!strcmp(searchStr,"ATTACK"))
				searchType = ATTACK;
		else if (!strcmp(searchStr,"MINIMAX"))
				searchType = MINIMAX;
		else if (!strcmp(searchStr,"ALPHABETA"))
				searchType = ALPHABETA;
		else if (!strcmp(searchStr,"EXPECTIMINIMAX"))
				searchType = EXPECTIMINIMAX;
		else 
				Util::Exit("Wrong search type/string in command line");

		this->maxSearchDepth = maxSearchDepth;
		this->index = index;
		if (index == 0)
				agentType = GOOD_GUY;
		else 
				agentType = BAD_GUY;

		curState = 0;
		nSteps = 0;
		score  = 0;
		isLose = false;
		isWin  = false;
}


// agent perceives the current environment
void Agent::perceive (){
		if (curState==0)
				curState = new State();
		else
				curState->updateState();
}

void Agent::act (){
		int newScore = env->act (index, selectedAction);
		if (index==0){
				score += newScore;
				if (newScore<-400)
						isLose = true;
				else if (newScore>400)
						isWin = true;
		} else{
				if (newScore < -400){
						agents[0]->score  += newScore;
						agents[0]->isLose = true;
				}
		}
		if (isWin || isLose)
				env->isGameEnd=true;
}


void Agent::printScore (){
		cout << "Score: " << score << endl;
		if (isWin)
				cout << "Agent wins!" << endl;
		else if (isLose)
				cout << "Agent loses!" << endl;
		if (isWin || isLose)
				env->isGameEnd=true;
}


// calls the corresponding search functions
void Agent::search(){
		if (searchType==RAND)
				randomSearch();
		else if (searchType==ATTACK)
				attackSearch();
		else if (searchType==MINIMAX)
				selectedAction = miniMaxDecision();
		else if (searchType==ALPHABETA)
				selectedAction = alphaBetaDecision();
		if (index==0){
				cout << "nTimesExpanded: " << env->nTimesExpanded << endl;
				cout << "selectedAction: " << env->actionNames[selectedAction] << endl;
		}
}

// is applicable only to enemy
// with 70%, random action
void Agent::attackSearch(){
		if (env->invTimer>=0 || rand()%10>7){
				randomSearch();
				return;
		}
		if (index==0){
				randomSearch();
				return;
		}
		vector<int> actions;
		env->getPossibleActions (curState->enemyPosList[index-1], actions);
		vector<int> dirActions;
		env->getDirectedActions (curState->enemyPosList[index-1], curState->agentPos, actions, dirActions);
		if (dirActions.size()==0)
				randomSearch();
		else{
				selectedAction = dirActions[rand()%dirActions.size()];
		}
}

// assigned selectedAction to one of the possible actions
void Agent::randomSearch(){
		vector<int> actions;
		if (index==0)
				env->getPossibleActions (curState->agentPos, actions);
		else
				env->getPossibleActions (curState->enemyPosList[index-1], actions);
		selectedAction = actions[rand()%actions.size()];
}

int Agent::minValue (int index, State *state, int depth){
		int minVal;
		// fill in
		return minVal;
}

// index is always 0
int Agent::maxValue (int index, State *state, int depth){
		int maxVal;
		// fill in
		return maxVal;
}

/**
	* Hints:
	* State *curState = new State(); // current state
	* env->getPossibleActions (0, curState, actions);
	* State *nextState = env->getNextState (0, curState, action);
	* int retVal = state->evaluate();
	*/
int Agent::miniMaxDecision(){
		int maxAction;
		int miniMaxVal;
		// fill in
		cout << "Minimax: "   << miniMaxVal << endl;
		return maxAction;
}


int Agent::minValue (int index, State *state, int &alpha, int &beta, int depth){
		int minVal;
		// fill in
		return minVal;
}


// index is always 0
int Agent::maxValue (int index, State *state, int &alpha, int &beta, int depth){
		int maxVal;
		// fill in
		return maxVal;
}

int Agent::alphaBetaDecision(){
		int maxAction;
		int miniMaxVal;
		// fill in
		cout << "Minimax: "   << miniMaxVal << endl;
		return maxAction;
}

int Agent::expectiMiniMaxDecision(){
		int expectiMaxAction;
		double expectiMiniMaxVal;
		// fill in 
		cout << "Expected Minimax: "   << expectiMiniMaxVal << endl;
		return expectiMaxAction;
}
